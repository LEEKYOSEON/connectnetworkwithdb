package com.suntel.connetdb;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    // url to get all products list
    private static String url_all_products = "http://ubuntu/TestDB_connectJson.php";
    private TextView mResultView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button mConnectServerButton = (Button) findViewById(R.id.btn_connectserver);
        mConnectServerButton.setOnClickListener(this);

        mResultView = (TextView)findViewById(R.id.txt_resultview);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_connectserver:
                    new ConnectTask().execute();
                break;
        }
    }

    private class ConnectTask extends AsyncTask<Void,Void,JSONObject> {

        @Override
        protected JSONObject doInBackground(Void... voids) {
            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            // getting JSON string from URL
            JSONObject json = jParser.makeHttpRequest(url_all_products, "GET", params);
            return json;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            try {
                JSONArray mDataJsonArray = jsonObject.getJSONArray("Total");
                for (int i = 0 ; i < mDataJsonArray.length() ; i++) {
                    setTextView(mResultView,mDataJsonArray.getString(i).toString() + "\n\n");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void setTextView(TextView textview, String str)
    {
        // Textview�� ����
        if(textview != null)
        {
            textview.append(str);
            textview.setSelected(true);
            AutoTextViewScroll(textview);
        }
    }

    private void AutoTextViewScroll(TextView textview)
    {
        // Textview�� ������ ���ڿ� �°� Scroll �̵�
        if(textview.getLineCount() > 8)
        {
            int Scrollamout = textview.getLayout().getLineTop(textview.getLineCount() - 8);
            if(Scrollamout > 8)
            {
                textview.scrollTo(0, Scrollamout);
            }
        }
    }
}
